<?php

namespace App\Form;

use App\Entity\Films;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilmType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Title')
            ->add('Year')
            ->add('Runtime')
            ->add('Poster')
            ->add('Plot')
            ->add('Released')
            ->add('Genre')
            ->add('Director')
            ->add('Language')
            ->add('Country')
            ->add('Awards')
            ->add('Type')
            ->add('DVD')
            ->add('Production')        

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'=> Films::class
            // Configure your form options here
        ]);
    }
}
