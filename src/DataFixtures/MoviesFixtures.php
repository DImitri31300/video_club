<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Movie;


class MoviesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

    	$content = '<p>'.join($faker->paragraphs(3),'</p> <p>').'</p>';

    	for ($i=0; $i<=10; $i++){
    		$movie= new Movie();
    		$movie->setTitle($faker->sentence())
    			//->setYear($faker->Date($format = 'Y-m-d', $max = 'now'))
    			//->setRuntime($faker->Time($format = 'H:i:s', $max = 'now'))
    			->setPoster($faker->imageUrl())
    			->setPlot($content)
    			->setReleased($faker->Name())
    			->setGenre($faker->word())
    			->setDirector($faker->lastName())
    			->setLanguage($faker->languageCode())
    			->setCountry($faker->country())
    			->setAwards($faker->colorName())
    			->setType($faker->word())
    			->setDVD($faker->fileExtension())
    			->setProduction($faker->company())
    			->setDisponibilite($faker->Boolean($chanceOfGettingTrue=50));
    			
    		//Pour faire persister notre film dans BDD
    		$manager->persist($movie);
    	}
        
    	//Ajoute nos films en BDD
        $manager->flush();

    }
}
