<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191120141445 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE movie (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, year DATE NOT NULL, runtime TIME NOT NULL, poster VARCHAR(255) NOT NULL, plot VARCHAR(255) NOT NULL, released VARCHAR(255) NOT NULL, genre VARCHAR(255) NOT NULL, director VARCHAR(255) NOT NULL, language VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, awards VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, dvd DATE NOT NULL, production VARCHAR(255) NOT NULL, disponibilite TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE films DROP FOREIGN KEY FK_CEECCA51F77D927C');
        $this->addSql('DROP INDEX IDX_CEECCA51F77D927C ON films');
        $this->addSql('ALTER TABLE films DROP panier_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE movie');
        $this->addSql('ALTER TABLE films ADD panier_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE films ADD CONSTRAINT FK_CEECCA51F77D927C FOREIGN KEY (panier_id) REFERENCES panier (id)');
        $this->addSql('CREATE INDEX IDX_CEECCA51F77D927C ON films (panier_id)');
    }
}
