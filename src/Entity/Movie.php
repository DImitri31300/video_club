<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MovieRepository")
 */
class Movie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

     /**
     * @ORM\Column(type="string", length=255)
     */
  
    private $Title;

    /**
     * @ORM\Column(type="date")
     */
    private $Year;

    /**
     * @ORM\Column(type="time")
     */
    private $Runtime;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Poster;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Plot;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Released;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Genre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Director;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Language;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Country;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Awards;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $DVD;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Production;

    /**
     * @ORM\Column(type="boolean")
     */
    private $disponibilite;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="movies")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Panier", inversedBy="movies")
     */
    private $panier;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->Title;
    }

    public function setTitle(string $Title): self
    {
        $this->Titre = $Title;

        return $this;
    }

    public function getYear(): ?string
    {
        return $this->Year;
    }

    public function setYear(string $Year): self
    {
        $this->Year = $Year;

        return $this;
    }

    public function getRuntime(): ?string
    {
        return $this-> Runtime;
    }

    public function setRuntime(string $Runtime): self
    {
        $this->duree = $Runtime;

        return $this;
    }

    public function getPoster(): ?string
    {
        return $this->Poster;
    }

    public function setPoster(string $Poster): self
    {
        $this->Poster = $Poster;

        return $this;
    }

    public function getPlot(): ?string
    {
        return $this->Plot;
    }

    public function setPlot(string $Plot): self
    {
        $this->Plot = $Plot;

        return $this;
    }

    public function getReleased(): ?string
    {
        return $this->Released;
    }

    public function setReleased(string $Released): self
    {
        $this->Released = $Released;

        return $this;
    }

    public function getGenre(): ?string
    {
        return $this->Genre;
    }

    public function setGenre(string $Genre): self
    {
        $this->Genre = $Genre;

        return $this;
    }
    public function getDirector(): ?string
    {
        return $this->Director;
    }

    public function setDirector(string $Director): self
    {
        $this->Director = $Director;

        return $this;
    }
    public function getLanguage(): ?string
    {
        return $this->Language;
    }

    public function setLanguage(string $Language): self
    {
        $this->Language = $Language;

        return $this;
    }
    public function getCountry(): ?string
    {
        return $this->Country;
    }

    public function setCountry(string $Country): self
    {
        $this->Country = $Country;

        return $this;
    }
    public function getAwards(): ?string
    {
        return $this->Awards;
    }

    public function setAwards(string $Awards): self
    {
        $this->Awards = $Awards;

        return $this;
    }
    public function getType(): ?string
    {
        return $this->Type;
    }

    public function setType(string $Type): self
    {
        $this->Type = $Type;

        return $this;
    }
    public function getDVD(): ?string
    {
        return $this->DVD;
    }

    public function setDVD(string $DVD): self
    {
        $this->DVD = $DVD;

        return $this;
    }
    public function getProduction(): ?string
    {
        return $this->Production;
    }

    public function setProduction(string $Production): self
    {
        $this->Production = $Production;

        return $this;
    }

    public function getAvailable(): ?bool
    {
        return $this->disponibilite;
    }

    public function setAvailable(bool $disponibilite): self
    {
        $this->disponibilite = $disponibilite;

        return $this;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    // public function setUser(?user $user): self
    // {
    //     $this->user = $user;

    //     return $this;
    // }

    public function getPanier(): ?panier
    {
        return $this->panier;
    }

    public function setPanier(?panier $panier): self
    {
        $this->panier = $panier;

        return $this;
    }
}