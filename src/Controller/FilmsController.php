<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Films;

class FilmsController extends AbstractController
{
	/**
	* @Route("/", name="visitor")
	*/
	public function visitor(){
		return $this->render('films/visitor.html.twig', [
			'title' => "Bonjour, si vous souhaitez avoir accès à nos films en location, merci de vous identifier"
		]);
	}

	/**
	* @Route("/home", name="home")
	*/
	public function home(){

		$repo = $this->getDoctrine()->getRepository(Films::class);

        $film = $repo->findAll();

		return $this->render('films/home.html.twig', [
			'title' => "Bienvenue sur notre site de location de Films",
			'controller_name' => 'FilmsController',
            'films' => $film,
		]);

	}

    /**
     * @Route("/films", name="films")
     */
    public function index()
    {
   
       
    }
}
