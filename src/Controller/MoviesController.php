<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Movie;

class MoviesController extends AbstractController
{
	/**
	* @Route("/", name="visitor")
	*/
	public function visitor(){
		return $this->render('movies/visitor.html.twig', [
			'title' => "Bonjour, si vous souhaitez avoir accès à nos films en location, merci de vous identifier"
		]);
	}

	/**
	* @Route("/home", name="home")
	*/
	public function home(){

		$repo = $this->getDoctrine()->getRepository(Movie::class);

        $movie = $repo->findAll();

		return $this->render('movies/home.html.twig', [
			'title' => "Bienvenue sur notre site de location de Films",
			'controller_name' => 'MoviesController',
            'films' => $movie,
		]);

	}
    /**
     * @Route("/movies", name="movies")
     */
    public function index()
    {
		return $this->render('movies/index.html.twig', [
            'controller_name' => 'MoviesController',
            ]);
    }
}
