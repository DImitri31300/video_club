<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Movie;
use App\Form\FilmType;

class ApiKeyController extends AbstractController
{
    /**
     * @Route("/query", name="apicall")
     */
    public function apicall(Request $request, ObjectManager $manager)
    {
        $form = $this->createForm(FilmType::class);
        $movie_search= $request->request->get('search_movie');
        
        $movie_data= file_get_contents("http://www.omdbapi.com/?t=".$movie_search."&apikey=afb23dce");
        $movie_data_decoded = json_decode($movie_data);
        var_dump($movie_data_decoded);

        $movie = new Movie();

        $movie->setTitle($movie_data_decoded->Title);
        $movie->setPlot($movie_data_decoded->Plot);
        $movie->setYear($movie_data_decoded->Year);
        $movie->setReleased($movie_data_decoded->Released);
        $movie->setRuntime($movie_data_decoded->Runtime);
        $movie->setGenre($movie_data_decoded->Genre);
        $movie->setDirector($movie_data_decoded->Director);
        $movie->setLanguage($movie_data_decoded->Language);
        $movie->setCountry($movie_data_decoded->Country);
        $movie->setAwards($movie_data_decoded->Awards);
        $movie->setPoster($movie_data_decoded->Poster);
        $movie->setType($movie_data_decoded->Type);
        $movie->setDVD($movie_data_decoded->DVD);
        $movie->setProduction($movie_data_decoded->Production);
        $movie->setAvailable(true);
        $manager->flush();
        
        return $this->render('movies/index.html.twig', [
            'controller_name' => 'MoviesController',
            ]);
    }
}
